
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */



require('spark-bootstrap');

require('./components/bootstrap');

import General from './components/Editor/General.vue';
import Email from './components/Editor/Email.vue';
import Features from './components/Editor/Features.vue';
import Image from './components/Editor/Image.vue';
import Video from './components/Editor/Video.vue';
import Coupons from './components/Editor/Coupons.vue';
import Tracking from './components/Editor/Tracking.vue';
import Lists from './components/Editor/Lists.vue';
import Control from './components/Editor/Control.vue';
import VueRouter from 'vue-router';

import Shopping from './components/Shopping.vue';

Vue.component('shopping', Shopping);

// Vue init
Vue.use(VueRouter);


const routes = [
    { path: '/general', component: General },
    { path: '/email', component: Email },
    { path: '/features', component: Features },
    { path: '/image', component: Image },
    { path: '/video', component: Video },
    { path: '/coupons', component: Coupons },
    { path: '/tracking', component: Tracking },
    { path: '/control', component: Control },
    { path: '/lists', component: Lists }
];

const SparkWrapper = require('./components/spark-wrapper');
const router = new VueRouter({routes});

var app = new Vue({
    mixins: [require('spark')],
    router,
});

