var base = require('kiosk/users');

Vue.component('spark-kiosk-users', {
    mixins: [base],

    mounted: function(){
        this.getUsers();
    },
    data: function() {
        return {
            subscribedUsers: [],
        }
    },
    methods:{
        getUsers(){
            this.$http.get('/api/allSubscribedUsers').then((response) =>{
                this.subscribedUsers = response.data;
            })
        }
    }
});
