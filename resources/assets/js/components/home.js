import Campaigns from "./Campaigns.vue";

Vue.component('home', {
    components: {
        Campaigns
    },
    props: ['user']
});
