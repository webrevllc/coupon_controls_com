<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="{{$campaign->promo_title}}"/>
    <meta property="og:image" content="{{json_decode($campaign->asinJSON)->LargeImage->URL}}"/>
    <meta property="og:site_name" content="CouponControls.com"/>

    <meta name="twitter:card" content="summary_large_image">
    {{--<meta name="twitter:site" content="@nytimes">--}}
    {{--<meta name="twitter:creator" content="@SarahMaslinNir">--}}
    <meta name="twitter:title" content="{{$campaign->promo_title}}">

    <meta name="twitter:image" content="{{json_decode($campaign->asinJSON)->LargeImage->URL}}">

    @if($campaign->features != null)
        @foreach(json_decode($campaign->features) as $feature)
            <meta property="og:description" content="{{$feature}}" />
            <meta name="twitter:description" content="{{$feature}}">
        @endforeach
    @else

    @endif

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>@yield('title', config('app.name'))</title>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/desoslide.min.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <style>
        a{color:#0066c0;}
        .inline{margin-top:-10px;margin-left:25px;}
        .mywell{
            border:1px solid #ccc;
            border-radius: 5px;
            padding:20px;
        }
        .container-fluid{
            padding-left:0px;
            padding-right:0px;
        }
        ul {
            list-style-type: none;
        }
        ul li{margin-bottom:10px;}
        .coupon-title{
            font-family:'Arial';
            font-size:21px;
        }
        .coupons-left{
            margin-top:40px;
            margin-right:50px;
        }
    </style>
</head>
<body>
    <div class="container-fluid" style="background:white; font-family: 'Arial' !important; color:#555!important;">
    <div class="page-header" style="background: #232f3e;margin-top:0px;">
        @if($campaign->campaign_logo_url != '')
            <img src="{{$campaign->campaign_logo_url}}" style="width:10%; padding:15px;" />
        @else
            {{--<img src="/img/amazon.jpg" style="width:30%" class="center-block"/>--}}
        @endif
        @if($campaign->show_coupons)
            <span class="pull-right">

                        <button class="btn btn-info coupons-left getCoupon"  type="button">
                            Coupons Remaining <span class="badge">{{$couponCount}}</span>
                        </button>
                    </span>
        @endif
    </div>
    <div class="clearfix"></div>
    {{--@include('promo.partials.email_form')--}}
    <div class="row">
        <div class="col-md-1">
            @if(!$campaign->use_video)
                <ul id="slideshow_thumbs">
                    @foreach( json_decode($campaign->asinJSON)->ImageSets->ImageSet as $image)
                        <li>
                            <a href="{{$image->LargeImage->URL}}">
                                <img src="{{$image->TinyImage->URL}}" alt="Thumbnail" width="75%" class="center-block">
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
        <div class="col-md-3">
            @if($campaign->use_video)
                <iframe width="100%" height="300px" src="https://www.youtube.com/embed/{{$campaign->videoID}}" frameborder="0" allowfullscreen></iframe>
            @else
                <div id="slideshow" style="margin-top:100px;"></div>
            @endif
        </div>
        <div class="col-md-6 ">
            <h4 class="coupon-title">
                {{$campaign->promo_title}}
            </h4>

            <i style="float:left" class="a-icon a-icon-star a-star-4-5"></i>

            @if($campaign->show_coupons)
                <a class="inline" href="#">Only {{count($campaign->coupons) - $campaign->claimed}} coupons left</a> | <a href="#">7 answered questions</a>
            @endif
            <div class="clearfix"></div>

            <hr style="margin-top:10px;">
            <table >
                <tr>
                    <td class="text-right col-md-2 nopx" >Price:</td>
                    <td class="text-left nopx"><span style="text-decoration: line-through;">${{$campaign->regular_price}}</span></td>
                </tr>
                <tr>
                    <td class="text-right  col-md-2 nopx" style="margin:0px;color:#b12704;">Sale:</td>
                    <td class="text-left nopx"><span style="font-size: 1.4em;color:#b12704;">${{$campaign->discount_price}}</span>
                        @if($campaign->is_prime)
                            <i class="a-icon a-icon-prime"></i>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="text-right  col-md-2 nopx">You Save:</td>
                    <td class="text-left nopx"><span style="color:#b12704;">${{($campaign->savings)}} ({{($campaign->savings * 100) / $campaign->regular_price}})%</span></td>
                </tr>

            </table>
            <h4 style="color:#008a00!important;font-size:17px;font-family:'Arial'">In Stock </h4>
            <ul>
                @if($campaign->features != null)
                    @foreach(json_decode($campaign->features) as $feature)
                        <li><h5>{{$feature}}</h5></li>
                    @endforeach
                @else

                @endif
            </ul>
            @if($campaign->reviews)
                <iframe src="{{preg_replace("/^http:/i", "https:", $amazon->reviews($campaign->asin))}}" width="100%" height="1500px" frameborder="0"></iframe>
            @endif
        </div>
        <div class="col-md-2">
            <div class="mywell">

                <div class="section-title">
                    <h3 style="margin-top:0px;color:#008a00!important;font-family:'Arial'; font-size:21px;">{{$campaign->promo_title}}</h3>
                    <h4>How Does This Work?</h4>
                    {{--@if(!Auth::check())--}}
                    {{--<h6>--}}
                    {{--<a href="/register">Sign up</a> to become a reviewer, it's only $10/yr. Then just login and return to this page and the coupon is all yours!--}}
                    {{--</h6>--}}
                    {{--<a href="#"><img src="/img/claim.png"/></a>--}}
                    {{--@else--}}
                    <h5>By clicking the "Claim My Coupon" button below you will receive a coupon for this item. </h5>
                    <h5>Purchase the item as soon as possible with the coupon (they do expire). </h5>
                    <a href="#" class="getCoupon"><img src="/img/claim.png"/></a>
                    <br>
                    <a href="{{$amazon->productUrl($campaign->asin)}}" target="_blank" class="text-center">View Product on Amazon</a>
                    {{--@endif--}}
                </div>
            </div>
        </div>
    </div>

    <!-- end section.our-stats -->

</div>
    <script src="/js/jquery.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/js/desoslide.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#slideshow').desoSlide({
                thumbs: $('#slideshow_thumbs li > a'),
                effect: 'none',
                overlay: 'none'
            });
            $('.getCoupon').click(function(){
                swal({
                        title: "Get Your Coupon!",
                        text: "Enter your email address",
                        type: "input",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Enter Your Email Address",
                        showLoaderOnConfirm: true,
                    },
                    function(inputValue){
                        if (inputValue === false) return false;

                        if (inputValue === "") {
                            swal.showInputError("You need to write something!");
                            return false
                        }

                        $.post("/api/getCoupon/{{$campaign->id}}", {email: inputValue}, function(data){
                            console.log(data);
                            if(data.error){
                                swal({
                                    title: "Problem",
                                    text: data.error,
                                    type: "error"
                                });
                            }else{
                                swal({
                                    title: "Nice",
                                    text: data.success,
                                    type: "success"
                                });
                            }

                        });

                    });
            })
        });

    </script>

</body>




