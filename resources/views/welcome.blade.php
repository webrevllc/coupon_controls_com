<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="p:domain_verify" content="db9ed3ec927572ce294a6cef73c188da"/>
    <meta name="keywords" content="amazon, coupon, control, single use distribution">

    <meta property="og:title" content="Amazon Single Use Coupon Automation"/>
    <meta property="og:image" content="https://couponcontrols.com/img/facebookimg.png"/>
    <meta property="og:site_name" content="CouponControls.com"/>
    <meta property="og:description" content="Amazon Single Use Coupon Automation Made Easy" />
    <meta property="og:url" content="https://couponcontrols.com" />

    <title>@yield('title', config('app.name'))</title>

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/elegant-icons/css/elegant-icons.min.css">
    <link rel="stylesheet" href="/assets/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="/assets/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="/assets/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <!--Colors-->
     <link rel="stylesheet" type="text/css" href="/assets/css/colors/blue.css">


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72547377-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body data-spy="scroll" data-offset="80">
<div class="navbar navbar-default navbar-fixed-top menu-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#home"><img src="/img/mono-logo.png" height="50px"/></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#home">Home</a></li>
                <li><a href="#features-section">Features</a></li>
                {{--<li><a href="#video-section">Video</a></li>--}}
                {{--<li><a href="#team-section">Team</a></li>--}}
                <li><a href="#contact-section">Contact</a></li>
                <li><a href="/login">Login</a></li>
                <li><a href="/register">Register</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- Top content -->
<div id="home" class="parallax top-content" data-stellar-background-ratio="0.3">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                    <h2>Automatically Distribute Your Single-use Coupons</br>Boost sales. Capture Emails.</h2>
                    <div class="description">
                    </div>
                    <div class="top-button">
                        <a class="btn btn-top btn-lg" href="/login"> Login</a>
                        <a class="btn btn-top-white btn-lg" href="/register"> Register</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Features -->
<div id="features-section" class="features-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 features">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 features-box1">
                <div class="icon">
                    <img src="/assets/img/icons/9.png" />
                </div>
                <h4>Easy Landing Pages</h4>
                <p>
                    Life is busy, CouponControls.com is easy. Go from sign up to product launch in under ten minutes.
                </p>
            </div>
            <div class="col-sm-4 features-box2">
                <div class="icon">
                    <img src="/assets/img/icons/1.png" />
                </div>
                <h4>Grow Your Customers</h4>
                <p>
                   Keep your customer list growing. Every time someone gets a coupon, you get their email address.
                </p>
            </div>
            <div class="col-sm-4 features-box3">
                <div class="icon">
                    <img src="/assets/img/icons/7.png" />
                </div>
                <h4>Mailchimp/Aweber Support</h4>
                <p>
                    Automate the process by adding your integration with Mailchimp or Aweber. Add new subscribers to your list today!
                </p>
            </div>
            <div class="col-sm-4 features-box4">
                <div class="icon">
                    <img src="/assets/img/icons/3.png" />
                </div>
                <h4>Coupons for Everyone</h4>
                <p>
                    <b>No third party site required.</b> We provide all of the hosting for your landing pages. Sign up and launch your campaign.
                </p>
            </div>
            <div class="col-sm-4 features-box5">
                <div class="icon">
                    <img src="/assets/img/icons/2.png" />
                </div>
                <h4>Beautiful Presentation</h4>
                <p>
                    Our landing pages were design specifically with Amazon customers in mind. We make it simple to distribute your coupons to your market.
                </p>
            </div>
            <div class="col-sm-4 features-box6">
                <div class="icon">
                    <img src="/assets/img/icons/8.png" />
                </div>
                <h4>Free Support</h4>
                <p>
                    CouponControls.com is so easy to use we guarantee your satisfaction. We are always standing by in case you need some help!
                </p>
            </div>

        </div>
    </div>
</div>

<div id="pricing-section" class="pricing-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 pricing">
                <h3>Choose the package</h3>
                <p>Currently we offer monthly and yearly pricing.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 toLeft">
                <div class="price-column">
                    <h2>Monthly</h2>
                    <h3><span>From</span> $39<span>.95 per month</span></h3>
                    <ul>
                        <li>Unlimited Campaigns</li>
                        <li>Unlimited Coupons</li>
                        <li>Integrates seemlessly with Mailchimp and Aweber</li>
                        <li>Many additional features</li>
                        <li><a href="/register" class="btn btn-top btn-lg">Purchase</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 toIn">
                <div class="price-column large">
                    <h2>Yearly</h2>
                    <h3><span>From</span> $439<span>.45 per year</span></h3>
                    <ul>
                        <li>Unlimited Campaigns</li>
                        <li>Unlimited Coupons</li>
                        <li>Integrates seemlessly with Mailchimp and Aweber</li>
                        <li>Get 1 month free</li>
                        <li><a href="/register" class="btn btn-top btn-lg">Purchase</a></li>
                    </ul>
                </div>
            </div>

        </div>

    </div>

</div>

<div id="faq-section" class="faq-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 faq-text">
                <h3>Frequently Asked Questions</h3>

                <div class="col-md-6 col-sm-6 col-xs-12 faq">
                    <div class="faq-box">
                        <h4 class="question"><i class="fa fa-question-circle"></i>How does CouponControls work?</h4>
                        <p class="answer">
                            Once you sign up with CouponControls you will create your first campaign.
                            We ask for the Amazon ASIN as well as a few other questions that will enable us to create your landing page for you.
                            Once you load up your coupons and launch the campaign you drive traffic to a unique url that you create.
                            Potential customers will submit their email address and receive a coupon. Simple!
                        </p>
                    </div>
                    <div class="faq-box">
                        <h4 class="question"><i class="fa fa-question-circle"></i>What do the landing pages look like?</h4>
                        <p class="answer">
                            We designed the landing page to look similar to an Amazon sales page. <a href="/demo">Check it out!</a>
                        </p>
                    </div>
                    <div class="faq-box">
                        <h4 class="question"><i class="fa fa-question-circle"></i>How many campaigns can I run?</h4>
                        <p class="answer">Currently our system is setup so that you can run as many campaigns and promote as many of your products simultaneously.</p>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 faq">
                    <div class="faq-box">
                        <h4 class="question"><i class="fa fa-question-circle"></i>Can I add Amazon reviews to my landing page?</h4>
                        <p class="answer">
                            Absolutely! We make that feature optional (in case you don't have many reviews yet).
                        </p>
                    </div>
                    <div class="faq-box">
                        <h4 class="question"><i class="fa fa-question-circle"></i>What email list prividers does CouponControls integrate with?</h4>
                        <p class="answer">CouponControls integrates directly with Mailchimp and Aweber. Now whenever someone claims one of your coupons they are added to the list of your choice. Use a different provider? No problem just download the CSV/Excel once your campaign is done and upload them wherever you want :)</p>
                    </div>
                    <div class="faq-box">
                        <h4 class="question"><i class="fa fa-question-circle"></i>What if I run out of coupons for my campaign, can I add more?</h4>
                        <p class="answer">
                            Yup! Simply go to your Coupon Management page and upload more coupons!

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Testimonials -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12 testimonials">
                <h3>What our customers have to say...</h3>

                <div class="col-md-12 text-center">
                    <div id="testimonial-carousel" class="owl-carousel owl-spaced">
                        <div>
                            <img src="/assets/img/testimonials/4.jpg" alt="" class="img-responsive img-thumbnail" />

                            <h4>
                                I have been using CouponControls.com for well over a year. I started slowly by launching only one product. As my business has grown this has become one of the most important tools in my toolkit. I have now launched more than 50 products SUCCESSFULLY with CouponControls.com
                            </h4>
                            <p>-Robert L.</p>
                        </div>
                        <div>
                            <img src="/assets/img/testimonials/4.png" alt="" class="img-responsive img-thumbnail" />

                            <h4>
                                It's simple, fast, and does what it says. Use CouponControls.com!
                            </h4>
                            <p>-Paul R.</p>
                        </div>
                        <div>
                            <img src="/assets/img/testimonials/5.png" alt="" class="img-responsive img-thumbnail" />

                            <h4>
                                We would not even consider some of the other "review groups" out there, they are a joke. And forget about posting on Facebook and waiting for some PMs, those days are HISTORY!!
                            </h4>
                            <p>-Andrez M</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Contact us -->
    <div id="contact-section" class="parallax contact-container" data-stellar-background-ratio="0.3">

        <div class="inner-contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 contact">
                        <h3>Contact Us</h3>
                        <p>
                            Questions? Comments? Want More information?
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group contact-form">
                        <h4>Send Message</h4>
                        <form method="post">
                            {{csrf_field()}}
                            <input type="text" name="email" placeholder="Email" class="form-control ">
                            <input type="text" name="subject" placeholder="Subject" class="form-control ">
                            <textarea name="message" class="form-control" placeholder="Message"></textarea>
                            <button type="submit" class="btn btn-contact">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- footer -->
    <div id="footer">
        <h3>Follow Us!</h3>
        {{--<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse suscipit tellus vitae tellus porta rutrum. Aliquam imperdiet nulla id scelerisque auctor.</p>--}}
        <div class="follow-us">
            <a href="https://facebook.com/couponcontrols" target="_blank" class="social-icon"><i class="fa fa-facebook"></i></a>
            {{--<a href="#" class="social-icon"><i class="fa fa-twitter"></i></a>--}}
            <a href="#" class="social-icon"><i class="fa fa-pinterest"></i></a>
        </div>
    </div>
    <!-- footer 2 -->
    <div id="footer2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright">&copy;
                        <script type="text/javascript">
                            //<![CDATA[
                            var d = new Date()
                            document.write(d.getFullYear())
                            //]]>
                        </script>
                        - CouponControls.com
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Javascript -->
    <script src="/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.backstretch.min.js"></script>
    <script src="/assets/js/retina-1.1.0.min.js"></script>
    <script src="/assets/js/jquery.easing.1.3.min.js"></script>
    <script src="/assets/js/jquery.stellar.min.js" type="text/javascript"></script>
    <script src="/assets/owl-carousel/owl.carousel.min.js"></script>
    <script src="/assets/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="/assets/js/script.js"></script>
    <script src="/assets/js/main.js"></script>


</body>

</html>

