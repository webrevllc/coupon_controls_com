@extends('spark::layouts.app')

@section('head')
    {{--<style>--}}
        {{--.col-md-4:nth-child(3n+1){--}}
            {{--clear:left--}}
        {{--}--}}
    {{--</style>--}}
@endsection
@section('content')

    <div class="container">
        <h1>Sorry, that promotion is not available.</h1>
        <h4>But check out these other offers that need reviews...</h4>

        @foreach($campaigns as $campaign)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">{{$campaign->campaign_name}}</h4>
                    </div>
                    <div class="card-block">
                        <img src="{{json_decode($campaign->asinJSON)->ImageSets->ImageSet[0]->LargeImage->URL}} " width="100%" height="350px"/>
                        <a href="/getcoupon/{{$campaign->promo_url}}"  class="card-link btn btn-block btn-primary">View Deal</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>


@stop

