@extends('spark::layouts.app')

@section('content')

    <home :user="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">
                @if(auth()->user()->current_billing_plan != 'REVIEWER')
                    <campaigns></campaigns>
                @else
                    <shopping></shopping>
                @endif
            </div>
        </div>
    </home>


@endsection
