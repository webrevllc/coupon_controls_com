@extends('spark::layouts.app')

@section('content')

    <div class="container">
        <div class="page-header">
            <div class="row">
                <div class="col-md-7">
                    <h1 style="margin-top:0px;">{{$campaign->campaign_name}} <small>{{$campaign->asin}}</small></h1>
                    <h4>{{$campaign->status}}</h4>
                </div>
                <div class="col-md-5">


                    <span class="pull-right" >
                        <a class="btn btn-info btn-lg" href="{{url('/preview/' . $campaign->promo_url)}}" target="_blank">Preview</a>
                    </span>

                    @if($campaign->status == "LAUNCHED")
                        <span class="pull-right" style="margin-right:5px;">
                            <a class="btn btn-primary btn-lg" href="{{url('/getcoupon/' . $campaign->promo_url)}}" target="_blank">View Live Page</a>
                        </span>
                    @endif
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked">
                    <router-link to="/general" tag="li" active-class="active"><a href="#">General Options</a></router-link>
                    {{--<router-link to="/email" tag="li" active-class="active"><a href="#">Email Settings</a></router-link>--}}
                    <router-link to="/features" tag="li" active-class="active"><a href="#">Features</a></router-link>
                    {{--<router-link to="/image" tag="li" active-class="active"><a href="#">Image</a></router-link>--}}
                    <router-link to="/video" tag="li" active-class="active"><a href="#">Video</a></router-link>
                    <router-link to="/coupons" tag="li" active-class="active"><a href="#">Coupons</a></router-link>
                    {{--<router-link to="/tracking" tag="li" active-class="active"><a href="#">Tracking/Analytics</a></router-link>--}}
                    <router-link to="/lists" tag="li" active-class="active"><a href="#">Mailing Lists</a></router-link>
                    <router-link to="/control" tag="li" active-class="active"><a href="#">Control Campaign</a></router-link>
                </ul>
            </div>
            <div class="col-md-9">
                <router-view :campaignid="{{ $campaign->id }}"></router-view>
            </div>
        </div>


    </div>



@endsection
