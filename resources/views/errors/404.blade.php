
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>0hh Website Template</title>
    <link href="/css/404.css" rel="stylesheet" type="text/css"  media="all" />
</head>
<body>
<!--start-wrap--->
<div class="wrap">
    <!---start-header---->
    <div class="header">
        <div class="logo">
            <h1><a href="#">Ohh</a></h1>
        </div>
    </div>
    <!---End-header---->
    <!--start-content------>
    <div class="content">
        <img src="/img/error-img.png" title="error" />
        <p><span><label>O</label>hh.....</span>The page you requested is not available.</p>
        <a href="/home">Back To Home</a>
        <div class="copy-right">
            <p style="visibility: hidden;">&#169 All rights Reserved | Designed by <a href="http://w3layouts.com/">W3Layouts</a></p>
        </div>
    </div>
    <!--End-Cotent------>
</div>
<!--End-wrap--->
</body>
</html>

