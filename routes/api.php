<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
|
*/

Route::group(['middleware' => 'auth:api',], function () {

    Route::get('/campaigns', 'CampaignController@index');
    Route::get('/campaign/{id}', 'CampaignController@getCampaign');
    Route::get('/campaignWithCoupons/{id}', 'CampaignController@campaignWithCoupons');
    Route::get('/allCampaigns', 'CampaignController@shopping');
    Route::get('/coupons/{id}', 'CampaignController@getCoupons');
    Route::get('/deleteCoupons/{id}', 'CouponController@deleteAllCoupons');
    Route::get('/deleteOneCoupon/{id}', 'CouponController@deleteOneCoupon');
    Route::get('/getAweberList/{id}', 'CampaignController@getAweberLists');
    Route::get('/launch/{id}', 'CampaignController@newLaunch');
    Route::get('/pause/{id}', 'CampaignController@newPause');
    Route::get('/deleteCampaign/{id}', 'CampaignController@newDelete');
    Route::get('/allSubscribedUsers', 'APIController@getAllSubscribedUsers');
    Route::get('/getShare/{campaign}', 'CampaignController@getShare');


    Route::post('/campaign', 'CampaignController@store');
    Route::post('/createCampaign', 'CampaignController@newStore');
    Route::post('/asinQuery', 'CampaignController@asinQuery');
    Route::post('/updateCampaign', 'CampaignController@updateCampaign');
    Route::post('/uploadLogo/{id}', 'CampaignController@uploadLogo');
    Route::post('/updateFeatures', 'CampaignController@updateFeatures');
    Route::post('/updateNewFeatures/{id}', 'CampaignController@updateNewFeatures');
    Route::post('/makeFeaturedImage/{id}', 'CampaignController@makeFeaturedImage');
    Route::post('/uploadImages/{id}', 'CampaignController@uploadImages');
    Route::post('/uploadCoupons/{id}', 'CampaignController@uploadCoupons');
    Route::post('/setMC/{id}', 'CampaignController@setMC');
    Route::post('/setAweberList/{id}', 'CampaignController@setNewAweberList');
    Route::post('/setMCList/{id}', 'CampaignController@setNewMCList');




});

Route::post('/getCoupon/{id}', 'CouponController@getNewCoupon');
