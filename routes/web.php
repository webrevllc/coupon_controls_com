<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Campaign;
use App\Coupon;

Route::get('/', 'WelcomeController@show');
Route::post('/contact', 'WelcomeController@contact');
Route::get('/demo', 'WelcomeController@demo');
Route::get('/getcoupon/{promoUrl}', 'CouponController@showLandingPage');

Route::group(['middleware' => ['auth', 'subscribed']], function () {
    Route::get('/home', 'HomeController@show')->middleware('subscribed');
    Route::get('/campaign/{id}/edit', 'CampaignController@edit')->middleware('auth', 'subscribed');
    Route::get('/campaigns/{id}/authorizeaweber', 'CampaignController@activateAweber')->middleware('auth', 'subscribed');
    Route::get('/getexcel/{id}', 'CampaignController@downloadCoupons');
    Route::get('/preview/{promoUrl}', 'CampaignController@showPreviewPage');
});

Route::get('/pause', function(){
    $campaigns = Campaign::all();
    foreach($campaigns as $c){
        $c->status = 'PAUSED';
        $c->save();
        echo $c->id .'<br>';
    }

});

Route::get('/update', function(\App\Contracts\Amazon $amazon){
    $campaigns = Campaign::where('created_at', '>', \Carbon\Carbon::createFromDate(2016,06,01))->get();
    foreach($campaigns as $c){
        $json = json_encode($amazon->getXML($c->asin));
        echo $c->id.'<br>';
        $c->asinJSON = json_encode((json_decode($json)->Item));
        $c->save();
        sleep(1);
    }

});

Route::get('/test', function(\App\Contracts\Amazon $amazon){
//    dd(env('AMAZON_ACCESS_KEY'));
        dd($amazon->reviews('B010Q57S62'));
//    $coupons = Coupon::where('assigned_to_ip', '')->get();
//
//    foreach ($coupons as $c){
//        $c->assigned_to_ip = null;
//        $c->save();
//    }
});
