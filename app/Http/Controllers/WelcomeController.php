<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Contracts\Amazon;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function show()
    {
        return view('welcome');
    }

    public function demo(Amazon $amazon)
    {

        $campaign = Campaign::with('coupons')->find(359);
//        dd($campaign);
//        $campaign = Campaign::with('coupons')->find(365);
        $couponCount = $campaign->coupons()->where('assigned_to', null)->get()->count();
        return view('demo', compact('campaign', 'amazon', 'couponCount'));
    }

    public function contact(Request $request)
    {
        return "success";
    }
}
