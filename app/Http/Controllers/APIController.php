<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Contracts\Amazon;
use App\Contracts\MailChimpContract;
use App\Coupon;
use App\Feature;
use App\Source;
use App\User;
use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;

class APIController extends Controller
{

    /**
     * @var Amazon
     */
    private $amazon;

    /**
     * APIController constructor.
     * @param Amazon $amazon
     */
    public function __construct(Amazon $amazon)
    {

        $this->amazon = $amazon;
    }
    public function getCampaigns(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $campaigns = Campaign::with('sources')->where('user_id', $user->id)->get();
//        $campaigns = $request->user()->campaigns;
//        $paused = $campaigns->filter(function($item){
//            return $item->status == "PAUSED";
//        });
//        $launched = $campaigns->filter(function($item){
//            return $item->status == "LAUNCHED";
//        });
//        $ready = $campaigns->filter(function($item){
//            return $item->status == "READY TO LAUNCH";
//        });
//        $needs = $campaigns->filter(function($item){
//            return $item->status == "NEEDS COUPONS";
//        });
//        $trashed = Campaign::where('user_id', \Auth::id())->onlyTrashed()->get();
//        $data = [
//            'paused' => $paused,
//            'launched' => $launched,
//            'ready' => $ready,
//            'needs' => $needs,
//            'trashed' => $trashed
//        ];
        return $campaigns;
    }

    public function createCampaign(Request $request, Amazon $amazon)
    {
        $fields = $request->input('creationFields');
//        $rules = [
//            'name' => 'required',
//            'asin' => 'required',
//            'url' => 'alpha_dash|unique:campaigns,promo_url'
//        ];
//
//        $this->validate($fields, $rules);
        $user = \Auth::user();

        $campaign = new Campaign;
        $campaign->campaign_name = $fields['name'];
        $campaign->user_id = $user->id;
        $campaign->asin = trim($fields['asin']);
        if(array_key_exists("url", $fields)){
            $campaign->promo_url = str_replace(' ', '-',strtolower($fields['url']));
        }
        try {
            if (! $amazon->getPrice($fields['asin'])) {
                throw new Exception("The field is undefined.");
            }
        }
        catch (Exception $e) {
            $campaign->status = 'NEEDS COUPONS';
            $user->campaigns()->save($campaign);
            return $campaign->id;
        }

        $campaign->regular_price = $amazon->getPrice($fields['asin']);
        $campaign->status = 'NEEDS COUPONS';
        $user->campaigns()->save($campaign);
        $this->makeFeatures($fields['asin'], $campaign->id);
        return $campaign->id;
    }

    public function getCampaign($id)
    {
        $campaign = Campaign::find($id);
        return $campaign;
    }

    public function updateCampaign($id, Request $request, Amazon $amazon)
    {
        $campaign = Campaign::find($id);
        $campaign->fill($request->all());
        $campaign->collect_names = $request->collect_names;
        $campaign->show_coupons = $request->show_coupons;
        $campaign->use_video = $request->use_video;
        $campaign->super_url = $campaign->returnSuperURL($amazon);
        $campaign->save();
        return $campaign;
    }

    public function updateEmail($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->email_name = $request->email_name;
        $campaign->email_address = $request->email_address;
        $campaign->email_subject = $request->email_subject;
        $campaign->facebook = $request->facebook;
        $campaign->twitter = $request->twitter;
        $campaign->campaign_email_body = $request->campaign_email_body;
        $campaign->email_ready = true;
        $campaign->save();
        return $campaign;
    }

    public function uploadLogo($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $file = $request->file('logo');
        $name = time() . $file->getClientOriginalName();
        Storage::disk('s3')->put($name, file_get_contents($file), 'public');
        $campaign->campaign_logo_url = $this->getFilePathAttribute($name);
        $campaign->save();
        return 'Done';
    }


    public function getFeaturedImages($id, Amazon $amazon)
    {
        $campaign = Campaign::find($id);
        try {
            $images = $amazon->returnAmazonImages($campaign->asin);
        } catch (Exception $e) {
            $images = [];
        }

        return $images;
    }

    public function selectImage($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->img_url = $request->image;
        $campaign->save();
    }

    public function updateVideo($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->videoID = $request->video;
        $campaign->save();
    }

    public function getCoupons($id)
    {
        $campaign = Campaign::with('coupons')->find($id);
        return $campaign;
    }

    public function uploadCoupons($id, Request $request)
    {
        $campaign = Campaign::find($id);
        if(count($campaign->coupons) > 0){
            $first = false;
        }else{
            $first = true;
        }
        $file = $request->file('coupons');
        $file = fopen($file, "r");

        $count = 0;
        while(!feof($file)){
            $line = fgets($file);
            if($line != ''){
                $coupon = new Coupon;
                $coupon->coupon = $line;
                $count++;
                $campaign->coupons()->save($coupon);
            }
        }
        fclose($file);
        if($first){
            $campaign->status = "READY TO LAUNCH";
            $campaign->save();
        }
        return json_encode($count);
    }

    public function deleteCoupon($id)
    {
        $coupon = Coupon::with('campaign')->find($id);
        if($coupon->campaign->user_id != \Auth::id()){
            return json_encode(array("error" => "That doesn't belong to you!"));
        }else{
            $coupon->delete();
            return json_encode(array("success" => "Deleted"));
        }
    }

    public function deleteAllCoupons($id)
    {
        $coupons = Coupon::where('campaign_id', $id)->get();
        foreach($coupons as $coupon){
            if($coupon->campaign->user_id != \Auth::id()){
                return json_encode(array("error" => "That doesn't belong to you!"));
            }else{
                $coupon->campaign->claimed = 0;
                $coupon->campaign->save();
                $coupon->delete();
            }
        }
        return json_encode(array("success" => "Deleted"));
    }

    public function downloadCoupons($id, Excel $excel)
    {
        $campaign = Campaign::find($id);
        $excel->create($campaign->id, function($e) use ($campaign){
            $e->sheet('CouponControls', function($sheet) use ($campaign) {
                $coupons = Coupon::where('campaign_id', $campaign->id)->get();
                $sheet->fromArray($coupons);

            });
        })->download('xls');
    }

    public function updateTracking($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->retargeting_pixel = $request->retargeting_pixel;
        $campaign->pixel = $request->pixel;
        $campaign->analytics = $request->analytics;
        $campaign->save();
        return $campaign;
    }

    public function getMCLists(Request $request, MailChimpContract $mailChimp)
    {
        $lists = $mailChimp->getLists($request->mc_api_key);

       return $lists;
    }

    public function getSources($id)
    {
        $sources = Source::where('campaign_id', $id)->get();
        return $sources;
    }

    public function saveSources($id, Request $request)
    {
        foreach($request->sources as $source){
            if(array_key_exists('status', $source)){
                $s = new Source();
                $s->label = $source['label'];
                $s->url = $source['url'];
                $s->campaign_id = $id;
                $s->save();
            }
        }
    }

    public function deleteSource($id)
    {
        $source = Source::with('campaign')->find($id);
        if($source->campaign->user_id == auth()->user()->id){
            $source->delete();
            return "success";
        }else{
            return "error";
        }
    }

    public function deleteFeature($id)
    {
        $feature = Feature::find($id);
        if($feature->campaign->user_id == auth()->user()->id){
            $feature->delete();
            return "success";
        }else{
            return "error";
        }
    }

    public function updateFeatures($campaign_id, Request $request)
    {
        $features = $request->features;
        foreach($features as $feature){
            if($feature['id'] == "new"){
                $f = new Feature;
                $f->campaign_id = $campaign_id;
                $f->feature = $feature['feature'];
                $f->save();
            }
            else{
                $f = Feature::find($feature['id']);
                $f->feature = $feature['feature'];
                $f->save();
            }
        }
    }

    public function getFeatures($id)
    {
        $features = Feature::where('campaign_id', $id)->get();
        return $features;
    }

    public function makeFeatures($asin, $campaign_id)
    {
        $features = $this->amazon->returnFeatures($asin);

        foreach($features as $feature){
            $f = new Feature;
            $f->campaign_id = $campaign_id;
            $f->feature = $feature;
            $f->save();
        }
    }


    public function addFeaturedImg($id, Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);

        $campaign = Campaign::findorfail($id);
        $file = $request->file('image');
        $name = time() . $file->getClientOriginalName();
        Storage::disk('s3')->put($name, file_get_contents($file), 'public');
        $campaign->additional_image = $this->getFilePathAttribute($name);
        $campaign->save();
        return 'Done';
    }


    public function getFilePathAttribute($value)
    {
        $disk = Storage::disk('s3');
        $bucket = Config::get('filesystems.disks.s3.bucket');
        if ($disk->exists($value)) {
            $request = $disk->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $value);
            return (string) $request;
        }
        return $value;
    }

    public function getAllSubscribedUsers()
    {
        $allUsers = User::all();
        $array = [];
        foreach($allUsers as $user){
            if($user->subscribed()){
                array_push($array, $user);
            }
        }
        return $array;
    }
}
