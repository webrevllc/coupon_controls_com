<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Events\PreSalesQuestion;
use App\Utilities\Curl;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class WebController extends Controller
{
    public function questions(Request $request)
    {
        $query = $request['query'];
//        new PreSalesQuestion($query);
        $mail = Mail::send('querymail', ['query' => $query], function ($message) use($query) {
            $message->from($query['email'], $query['first'] . ' ' . $query['last'] );
            $message->to('pleasereply@couponcontrols.com');
        });
return $mail;
    }

    public function askQ(Request $request, Curl $curl)
    {
        //ensure human
        $response = json_decode($curl->post('https://www.google.com/recaptcha/api/siteverify', [
            'secret' => config('services.recaptcha.secret'),
            'response' => $request->input('g-recaptcha-response'),
            'remoteip' => $request->ip(),
        ]));

        if(! $response->success){
            abort(400);
        }else{
//            return 'got it';
            $query = $request->all();
            $mail = Mail::send('querymail', ['query' => $query], function ($message) use($query) {
                $message->from($query['email'], $query['first'] . ' ' . $query['last'] );
                $message->to('help@couponcontrols.com');
            });
            return redirect()->back()->withInput()->with('success', 'Email Sent!');
        }
    }

    public function helppage()
    {
        return view('admin.help');
    }

    public function home()
    {
        $coupons = (Coupon::all()->count()*7);
        if(Input::has('ap_id')){
            Cookie::queue('ap_id', Input::get('ap_id'), 60);
            return view('welcome', compact('coupons'));
        }else{
            return view('welcome', compact('coupons'));
        }
    }
}
