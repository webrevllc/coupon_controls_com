<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Contracts\AweberContract;
use App\Contracts\MailChimpContract;
use App\Coupon;
use App\Events\CampaignLaunched;
use App\Events\CampaignPaused;
use App\Feature;
use App\Contracts\Amazon;
use Chencha\Share\ShareFacade;
use CS_REST_Subscribers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;

class CampaignController extends Controller
{

    /**
     * @var AweberContract
     */
    private $aweberContract;
    /**
     * @var MailChimp
     */
    private $mc;
    /**
     * @var Amazon
     */
    private $amazon;
    /**
     * @var Excel
     */
    private $excel;

    /**
     * CampaignController constructor.
     * @param AweberContract $aweberContract
     * @param MailChimpContract|MailChimp $mc
     * @param Amazon $amazon
     * @param Excel $excel
     */
    public function __construct(AweberContract $aweberContract, MailChimpContract $mc, Amazon $amazon, Excel $excel)
    {
        $this->aweberContract = $aweberContract;
        $this->mc = $mc;
        $this->amazon = $amazon;
        $this->excel = $excel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $campaigns = auth()->user()->campaigns;
//        foreach($campaigns as $campaign){
//
//            if($campaign->asinJson == null || $campaign->asinJSON ==''){
//                $product = $this->amazon->getXML($campaign->asin);
//                $campaign->asinJSON = json_encode($product['Item']);
//            }
//
//            if($campaign->share_links == null){
//                $sharelinks = json_encode(ShareFacade::load(url('/getcoupon/' . $campaign->promo_url), $campaign->promo_title)->services('facebook', 'gplus', 'twitter'));
//                $campaign->share_links = $sharelinks;
//            }
//
//            $campaign->save();
//        }
        return $campaigns;

    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $user = Auth::user();
        $campaign = new Campaign;
//        $campaign->campaign_name = $request->title;
        $campaign->user_id = $user->id;
        $campaign->asin = $data['ASIN'];


        if(auth()->user()->current_billing_plan != 'CC-Pro-M' || auth()->user()->current_billing_plan != 'CC-Pro-Y'){
            $campaign->is_featured = true;
        }else{
            $campaign->is_featured = false;
        }

        if(isset($data['ItemAttributes']['ListPrice'])){
            $campaign->regular_price = ($data['ItemAttributes']['ListPrice']['Amount']) /100;
        }

        $campaign->status = 'NEEDS COUPONS';
        $campaign->asinJSON = json_encode($request->all());
        $user->campaigns()->save($campaign);
        return $campaign;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $campaign = Campaign::find($id);
        if (Gate::allows('ShowCampaign', $campaign)) {
            return view('campaigns.edit', compact('campaign'));
        }else{
            abort(403, "You do not have access to that campaign!");
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
//        dd($request->all());
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->campaign_name = $request->campaign_name;
        $campaign->promo_title = $request->promo_title;
        $campaign->regular_price = $request->regular_price;
        $campaign->discount_price = $request->discount_price;
        $campaign->end_date = $request->end_date;
        $campaign->reviews = $request->has('reviews');
        $campaign->filter_ips = $request->has('filter_ips');
        $campaign->filter_emails = $request->has('filter_emails');
        $campaign->use_video = $request->has('use_video');
        $campaign->show_coupons = $request->has('show_coupons');
        $campaign->collect_names = $request->has('collect_names');
        $campaign->super_url = $request->super_url;
        $campaign->save();
//        flash()->success('Campaign Updated', 'Have you selected your featured image yet?');
        return redirect()->back();
    }



    private function checkForASIN($id)
    {
        try{
            $campaign = Campaign::findorfail($id);
            $this->authorize('ShowCampaign', $campaign);
            if($campaign->asin != ''){
                return true;
            }else{
                return false;
            }
        }catch(ModelNotFoundException $m){
            return "There was an error and the CouponControls team has been notified";
        }

    }

    public function selectList(Request $request)
    {
        $id = $request->lists;
        $campaign = Campaign::find($request->campaignId);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->mc_list = $id;
        $campaign->save();
//        flash()->success('HEY!', 'Nice list :)');
        return redirect()->back();
    }


    public function launch($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
//        dd($campaign->goForLaunch());
        if($campaign->goForLaunch()){
            $campaign->status = 'LAUNCHED';
            $campaign->save();
            event(new CampaignLaunched(Auth::user(), $campaign));
            return redirect('/admin/campaigns');
        }else{
            return redirect('/admin/campaigns/'. $campaign->id.'/edit');
        }

    }

    public function verifyUrl(Request $request)
    {
        $userCampaigns = Auth::user()->campaigns;
        foreach($userCampaigns as $campaign){
            if($request->url == $campaign->promo_url){
                return 'true';
            }else{
                $c2 = Campaign::where('promo_url','=', $request->url)->get();
                if($c2->isEmpty()){
                    return 'true'; //verified show green
                }else{
                    return 'false'; //show red
                }
            }
        }
        return 'true';
    }

    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        Campaign::destroy($id);
        return redirect()->back();
    }

    public function newDelete($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        Campaign::destroy($id);
        return 'success';
    }

    public function restore($id)
    {
        $campaign = Campaign::withTrashed()->find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->restore();
        return redirect()->back();
    }

    public function pause($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->status = "PAUSED";
        $campaign->save();
        return redirect()->back();
    }

    public function images($id, Amazon $amazon)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        return view('admin.campaigns.images', compact('campaign'));
    }
    public function tracking($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        return view('admin.campaigns.tracking', compact('campaign'));
    }
    public function coupons($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        return view('admin.campaigns.coupons', compact('campaign'));
    }


    public function email($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        return view('admin.campaigns.email', compact('campaign'));
    }

    public function features($id)
    {
        $campaign = Campaign::with('features')->find($id);
        $this->authorize('ShowCampaign', $campaign);
        if( count($campaign->features) == 0){
            $features = $this->amazon->returnFeatures($campaign->asin);
            foreach($features as $feature){
                $f = new Feature;
                $f->campaign_id = $campaign->id;
                $f->feature = $feature;
                $f->save();
            }
        }
        return view('admin.campaigns.features', compact('campaign'));
    }

    public function logo($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        return view('admin.campaigns.logo', compact('campaign'));
    }

    public function uploadLogo($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $file = request()->file('logo');
        $path = $file->store('logos', 's3');
        $campaign->campaign_logo_url = Storage::disk('s3')->url($path);
        $campaign->save();
        return 'Done';
    }

    public function getVideo($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        return view('admin.campaigns.video', compact('campaign'));
    }

    public function showSources($id)
    {
        $campaign = Campaign::with('sources')->find($id);
        $this->authorize('ShowCampaign', $campaign);
        return view('admin.campaigns.sources', compact('campaign'));
    }

    public function getListsPage($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        if($campaign->mc_api_key != '')
        {
            if($this->mc->getLists($campaign->mc_api_key)){
                $mclists = $this->mc->getLists($campaign->mc_api_key);
            }else{
                $mclists = [];
            }
        }else
        {
            $mclists = '';
        }

        if($campaign->aweber_key != '')
        {
            $lists = $this->getAweberLists($id);
        }
        else{
            $lists = [];
        }

        return view('admin.campaigns.lists', compact('campaign', 'mclists', 'lists'));
    }

    public function activateAweber($campaign_id)
    {
        $ksaid = $this->aweberContract->display_access_tokens();
        if(!empty($ksaid))
        {
            $c = Campaign::find($campaign_id);
            $c->aweber_account_id = $ksaid['account_id'];
            $c->aweber_key = $ksaid['key'];
            $c->aweber_secret = $ksaid['secret'];
            $c->save();
            $this->getAweberLists($campaign_id);
        }
        Session::regenerateToken();
        return redirect('/campaign/'.$campaign_id.'/edit#/lists');
    }

    public function getAweberLists($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $account = $this->aweberContract->getAccount($campaign->aweber_key, $campaign->aweber_secret);
        $lists = $this->aweberContract->display_available_lists($account);
        return $lists;
    }

    public function setAweberList($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->aweber_list_id = $request->aweber;
        $campaign->save();

//        flash()->success('You did it!', 'Aweber List Saved!!');
        return redirect()->back();
    }
    public function setMCAPI($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->mc_api_key = trim($request->mc_api_key);
        $campaign->save();

        return redirect()->back();
    }

    public function setMCList($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $mc_list = $request->mc_list;
        $campaign->mc_list = $mc_list;
        $campaign->save();

        return redirect()->back();
    }

    public function clearAweber($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->aweber_list_id = '';
        $campaign->save();
        return redirect()->back();
    }

    public function clearMC($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->mc_list = '';
        $campaign->save();
        return redirect()->back();
    }

    public function lists()
    {
        $wrap = new CS_REST_Subscribers('82265836d9efa58b26df914f191d3142', ['api_key' => 'c0f878898616bfe0b885fdba222db8f9']);
        $result = $wrap->add(array(
            'EmailAddress' => 'yo@dudes.com',
            'Name' => 'Subscriber name'
        ));
        dd($result);
    }

    public function newStore(Request $request)
    {
        $data = $request->Item;
        $title = $request->title;
        $user = Auth::user();
        $campaign = new Campaign();
        $campaign->asinJSON = json_encode($data);
        $campaign->campaign_name = $title;
        $campaign->asin = $data['ASIN'];
        $campaign->regular_price = $data['ItemAttributes']['ListPrice']['Amount'] /100;
        $campaign->user_id = $user->id;
        if(auth()->user()->current_billing_plan != 'CC-Pro-M' || auth()->user()->current_billing_plan != 'CC-Pro-Y'){
            $campaign->is_featured = true;
        }else{
            $campaign->is_featured = false;
        }

        $campaign->status = 'NEEDS COUPONS';
        $user->campaigns()->save($campaign);

        $campaign->promo_url = $campaign->id.'-'. time();
        $campaign->save();
        return $campaign->id;
    }

    public function shopping()
    {
        return Campaign::where('status', 'LAUNCHED')->inRandomOrder()->get();
    }

    public function asinQuery(Request $request)
    {
        $asin = $request['ASIN'];
        $name = $request['name'];
        $product = $this->amazon->getXML($asin);
        $obj = [
            "Product" => $product,
            "name" => $name
        ];
        return $obj;
    }

    public function getCampaign($id)
    {
        $campaign = Campaign::find($id);
        return $campaign;
    }

    public function campaignWithCoupons($id)
    {
        $campaign = Campaign::with('coupons')->find($id);
        return $campaign;
    }

    public function calculateSavings($campaign)
    {
        $r = $campaign->regular_price;
        $d = $campaign->discount_price;
        $campaign->savings = $r - $d;
        $campaign->save();
    }

    public function updateCampaign(Request $request)
    {
        $campaign = Campaign::find($request['id']);
        $campaign->fill($request->all());
        $this->calculateSavings($campaign);
        if(!isset($campaign->promo_url)){
            $campaign->promo_url = $campaign->id.'-'.time();
        }
        $campaign->save();
        return $campaign;
    }

    public function updateFeatures(Request $request)
    {
        $campaign = Campaign::find($request->id);
        $json =  json_decode($campaign->asinJSON);
        $campaign->features = json_encode($json->ItemAttributes->Feature);
        $campaign->save();
        return $campaign->features;
    }

    public function updateNewFeatures($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->features = $request['data'];
        $campaign->save();
        return $campaign->features;
    }
    public function makeFeaturedImage($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $campaign->featured_image_index = $request['value'];
        $campaign->img_url = $request['url'];
        $campaign->save();
        return $campaign->features;
    }

    public function getCoupons($id)
    {
        $coupons = Coupon::where('campaign_id', $id)->get();
        return $coupons;
    }

    public function uploadCoupons($id, Request $request)
    {
        $campaign = Campaign::find($id);
        if(count($campaign->coupons) > 0){
            $first = false;
        }else{
            $first = true;
        }
        $file = $request->file('coupons');
        $file = fopen($file, "r");

        $count = 0;
        while(!feof($file)){
            $line = fgets($file);
            if($line != ''){
                $coupon = new Coupon;
                $coupon->coupon = $line;
                $count++;
                $campaign->coupons()->save($coupon);
            }
        }
        fclose($file);
        if($first){
            $campaign->status = "READY TO LAUNCH";
            $campaign->save();
        }
        return json_encode($count);
    }

    public function setMC($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->mc_api_key = $request['mc-api'];
        $campaign->save();
        if($this->mc->getLists($campaign->mc_api_key)){
            $mclists = $this->mc->getLists($campaign->mc_api_key);
        }else{
            $mclists = [];
        }

        return $mclists;
    }

    public function setNewMCList($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->mc_list = $request['mc-list'];
        $campaign->mc_list_name = $request['mc-list-name'];
        $campaign->save();
        return $request['mc-list-name'];
    }

    public function setNewAweberList($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->aweber_list_id = $request['aw-list'];
        $campaign->aweber_list_name = $request['aw-list-name'];
        $campaign->save();
        return $campaign;
    }

    public function newLaunch($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->status = 'LAUNCHED';
        $sharelinks = json_encode(ShareFacade::load(url('/getcoupon/' . $campaign->promo_url), $campaign->promo_title)->services('facebook', 'gplus', 'twitter'));
        $campaign->share_links = $sharelinks;
        $campaign->save();
        event(new CampaignLaunched($campaign, Auth::user()));

        return $sharelinks;
    }

    public function newPause($id)
    {
        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $campaign->status = 'PAUSED';
        $campaign->save();
        event(new CampaignPaused($campaign, Auth::user()));
        return "success";
    }

    public function uploadImages($id)
    {

        $campaign = Campaign::find($id);

        $this->authorize('ShowCampaign', $campaign);
        $file = request()->file('image');
        $path = $file->store('logos', 's3');
        $url = Storage::disk('s3')->url($path);
        if($campaign->images == null){
            $newArray = array();
            $campaign->featured_images = array_push($newArray, json_encode((array)$url));
        }else{
            $campaign->featured_images = array_push($campaign->featured_images, json_encode((array)$url));
        }

        $campaign->save();
        return $campaign->images;
    }

    public function downloadCoupons($id)
    {

        $campaign = Campaign::find($id);
        $this->authorize('ShowCampaign', $campaign);
        $this->excel->create($campaign->id, function($e) use ($campaign){
            $e->sheet('CouponControls', function($sheet) use ($campaign) {
                $coupons = Coupon::where('campaign_id', $campaign->id)->get();
                $sheet->fromArray($coupons);

            });
        })->download('xls');
    }

    public function showPreviewPage($promoUrl, Amazon $amazon)
    {
        $campaign  = Campaign::where('promo_url', $promoUrl)->first();
        $couponCount = $campaign->coupons()->where('assigned_to', null)->get()->count();
        $this->authorize('ShowCampaign', $campaign);
        return view('previewPage', compact('campaign', 'amazon', 'couponCount'));
    }

    public function getShare(Campaign $campaign)
    {
        return ShareFacade::load( url('/getcoupon/' . $campaign->promo_url), $campaign->promo_title)->services('facebook', 'gplus', 'twitter');
    }
}
