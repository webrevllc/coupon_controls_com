<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Contracts\Amazon;
use App\Contracts\AweberContract;
use App\Contracts\MailChimpContract;
use App\Coupon;
use App\Mail\HeresYourCoupon;
use App\Source;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Mail;


class CouponController extends Controller
{


    /**
     * @var Amazon
     */
    private $amazon;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var MailChimpContract
     */
    private $mailChimpContract;
    /**
     * @var AweberContract
     */
    private $aweberContract;

    /**
     * CouponController constructor.
     * @param Amazon $amazon
     * @param Request $request
     * @param MailChimpContract $mailChimpContract
     * @param AweberContract $aweberContract
     */
    public function __construct(Amazon $amazon, Request $request, MailChimpContract $mailChimpContract, AweberContract $aweberContract)
    {
        $this->amazon = $amazon;
        $this->request = $request;
        $this->mailChimpContract = $mailChimpContract;
        $this->aweberContract = $aweberContract;
    }
    public function show($id)
    {
        $campaign = Campaign::with('coupons', 'sources')->find($id);
        //if()  check if campaign belongs to user
        return view('admin.coupon', compact('campaign'));
    }

    public function lookupCoupon($promo_url, Amazon $amazon, Request $request)
    {
        try{
            $campaigner = Campaign::where('promo_url', $promo_url)->firstorfail();
            $couponCount = Coupon::where('campaign_id', $campaigner->id)->get()->count();
            if($request->has('source')){
                $sources = Source::where('campaign_id', $campaigner->id)->get();
                foreach($sources as $source){
                    if(substr($source->url, -1) == $request->get('source')){
                        $source->visits++;
                        $source->save();
                    }
                }
            }

            if($couponCount == $campaigner->claimed){
                $relatedItems = $amazon->related($campaigner->asin);
                return view('promo.ended2', compact('relatedItems'));
            }else{
                try{
                    $campaign = Campaign::with('features')->where('promo_url', $promo_url)->firstorfail();

                    if($campaign != null){
                        if($campaign->status == 'LAUNCHED'){
                            return view('promo.show', ['campaign' => $campaign, 'amazon' => $amazon]);
                        }
                        $relatedItems = $amazon->related($campaign->asin);
                        return view('promo.ended2', compact('relatedItems'));
                    }
                    $relatedItems = $amazon->related($campaign->asin);
                    return view('promo.ended2', compact('relatedItems'));
                }catch(ModelNotFoundException $m){
                    $relatedItems = $amazon->related('B00005UP2P');
                    return view('promo.ended2', compact('relatedItems'));
                }
            }
        }catch(ModelNotFoundException $m){
            $relatedItems = $amazon->related('B00005UP2P');
            return view('promo.ended2', compact('relatedItems'));
        }
    }

    public function deleteOne(Request $request)
    {
        $coupon = Coupon::with('campaign')->find($request->get('id'));
        if($coupon->campaign->user_id != \Auth::id()){
            flash()->error('Sorry', 'That is not yours!');
            return redirect()->back();
        }else{
           --$coupon->campaign->claimed;
            $coupon->campaign->save();
            $coupon->delete();
//            flash()->success('Sorry', 'That is not yours!');
            return redirect()->back();
        }
    }

    public function skipLandingPage($promo_url)
    {
        $campaign = Campaign::with('features')->where('promo_url', $promo_url)->firstorfail();
        if($campaign->status == "LAUNCHED" ){
            try{
                $coupon = Coupon::where('campaign_id', $campaign->id)->where('assigned_to_ip', $this->request->ip())->firstorfail();
                $data = ['coupon' => $coupon->coupon, 'amazon' => $this->amazon, 'campaign' => $campaign];
                return view('promo.coupon', $data);
            }catch(ModelNotFoundException $m){
                try{
                    $coupon = Coupon::where('campaign_id', $campaign->id)->where('assigned_to_ip', '')->firstorfail();
                    $coupon->assigned_to_ip = $this->request->ip();
                    $coupon->save();
                    $data = ['coupon' => $coupon->coupon, 'amazon' => $this->amazon, 'campaign' => $campaign];
                    return view('promo.coupon', $data);
                }catch(ModelNotFoundException $m){
                    //redirect to campaign over page
                    $relatedItems = $this->amazon->related($campaign->asin);
                    return view('promo.ended2', compact('relatedItems'));
                }
            }
        }else{
            $relatedItems = $this->amazon->related($campaign->asin);
            return view('promo.ended2', compact('relatedItems'));
        }

    }

    public function couponSent($id)
    {
        $campaign = Campaign::find($id);
        return view('promo.coupon_sent', compact('campaign'));
    }



    public function showLandingPage($promoUrl, Amazon $amazon)
    {
        $campaign  = Campaign::where('promo_url', $promoUrl)->first();

        if($campaign->status != "LAUNCHED" || $campaign == null){
            $campaigns = Campaign::where('status', 'LAUNCHED')->where('created_at', '>', Carbon::createFromDate(2016,12,01))->get();
            return view('ended', compact('campaigns'));
        }else{
            $couponCount = $campaign->coupons()->where('assigned_to_ip', null)->get()->count();
            return view('landingPage', compact('campaign', 'amazon', 'couponCount'));
        }

    }

    public function deleteAllCoupons($id)
    {
        Coupon::where('campaign_id', $id)->delete();
        $campaign = Campaign::find($id);
        $campaign->status = "NEEDS COUPONS";
        $campaign->save();
    }

    public function deleteOneCoupon($id)
    {
        $coupon = Coupon::find($id);
        $campaign = Campaign::find($coupon->campaign_id);
        $coupon->delete();
        $count = $campaign->coupons()->count();

        if($count == 0){
            $campaign->status = "NEEDS COUPONS";
            $campaign->save();
        }



    }

    public function validateIPForCampaign($ip, $campaign)
    {
        return $campaign->coupons()->where('assigned_to_ip', $ip)->get()->isEmpty();
    }

    public function validateEmailForCampaign($email, $campaign)
    {
        return $campaign->coupons()->where('assigned_to', $email)->get()->isEmpty();
    }

    public function getNextCoupon($campaign)
    {
        $coupon = Coupon::where([
            ['campaign_id', $campaign->id],
            ['assigned_to_ip', null]
        ])->first();

        return $coupon;
    }

    public function assignCouponToIPAndEmail($coupon, $ip, $email, $f = null, $l = null)
    {
        $coupon->assigned_to_ip = $ip;
        $coupon->assigned_to= $email;
        $coupon->first_name= $f;
        $coupon->last_name= $l;
        $coupon->save();
    }

    public function shouldAddAddressToMC($campaign)
    {
        return !is_null($campaign->mc_list);
    }

    public function shouldAddAddressToAW($campaign)
    {
        return !is_null($campaign->aweber_list_id);
    }

    public function addAddressToMailChimp($email, $campaign)
    {
        $this->mailChimpContract->addEmailToList($email, $campaign->mc_list, $campaign->mc_api_key);
    }

    public function addAddressAndUserToMailChimp($email, $first_name, $last_name, $campaign)
    {
        $this->mailChimpContract->addEmailAndUserToList($email, $first_name, $last_name, $campaign->mc_list, $campaign->mc_api_key);
    }

    public function addAddressToAweber($email_address, $campaign)
    {
        $account = $this->aweberContract->getAccount($campaign->aweber_key, $campaign->aweber_secret);
        $this->aweberContract->addEmailToList($account, $campaign->aweber_list_id, $email_address);
    }

    public function addAddressAndUserToAweber($email_address, $first_name, $last_name, $campaign)
    {
        $account = $this->aweberContract->getAccount($campaign->aweber_key, $campaign->aweber_secret);
        $this->aweberContract->addEmailAndUserToList($account, $campaign->aweber_list_id, $email_address, $first_name . ' ' . $last_name);
    }


    public function campaignHasEnoughCoupons($campaign)
    {
        return $campaign->coupons()->where('assigned_to_ip', null)->count() > 0;

    }
    public function getNewCoupon($id, Request $request)
    {

        $campaign = Campaign::findorfail($id);
        $email_address = $request->email;
        $ip = request()->ip();

        if (filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
            if($this->validateIPForCampaign($ip, $campaign)){
                if($this->validateEmailForCampaign($email_address, $campaign)){
                    if($this->campaignHasEnoughCoupons($campaign)){
                        //get next available coupon
                        $coupon = $this->getNextCoupon($campaign);

                        //assign it
                        $this->assignCouponToIPAndEmail($coupon, $ip, $email_address, $request->first_name, $request->last_name);

                        //email it
                        Mail::to($email_address)->send(new HeresYourCoupon($coupon, $campaign));

                        //check for Aweber/MC Integration and add address to List
                        if($this->shouldAddAddressToMC($campaign)){
                            if(array_key_exists("first_name",$request->all())){
                                $this->addAddressAndUserToMailChimp($email_address, $request->first_name, $request->last_name, $campaign);
                            }else{
                                $this->addAddressToMailChimp($email_address, $campaign);
                            }

                        }
                        if($this->shouldAddAddressToAW($campaign)){
                            if(array_key_exists("first_name",$request->all())){
                                $this->addAddressAndUserToAweber($email_address, $request->first_name, $request->last_name, $campaign);
                            }else{
                                $this->addAddressToAweber($email_address, $campaign);
                            }

                        }
                    }else{
                        //pause campaign
                        $campaign->status = "PAUSED";
                        $campaign->save();
                        return ["error" => "Sorry, no more coupons for this campaign!"];
                    }


                    //return success message
                    return ["success" => "A coupon has been assigned to $ip and has been emailed to $email_address. Check your email!"];
                }else{
                    return ["error" => "We hae already emailed a coupon to that email address"];
                }
            }else{
                return ["error" => "Someone from that IP has already claimed a coupon"];
            }

        } else {
            return ["error" => "This ($email_address) email address is considered invalid.\n"];

        }


    }
}
