<?php

namespace App\Http\Controllers;

use App\Claim;
use App\Contracts\AweberContract;
use App\Contracts\MailChimpContract;
use App\Coupon;
use App\Campaign;
use App\Contracts\Amazon;
use App\Events\ActivateClaim;
use App\Source;
use Event;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Events\CouponWasClaimed;
use Illuminate\Support\Facades\Log;


class ClaimController extends Controller
{

    /**
     * @var MailChimp
     */
    private $mc;
    /**
     * @var AweberContract
     */
    private $aweberContract;
    /**
     * @var Amazon
     */
    private $amazon;

    /**
     * ClaimController constructor.
     *
     * @param MailChimp $mc
     * @param AweberContract $aweberContract
     * @param Amazon $amazon
     */
    public function __construct( MailChimpContract $mc, AweberContract $aweberContract, Amazon $amazon ) {

        $this->mc = $mc;
        $this->aweberContract = $aweberContract;
        $this->amazon = $amazon;
    }
    public function validateClaimRequest($campaign, $collector, $request)
    {
        $countEmails = count($campaign->coupons->where('assigned_to', $collector->email));
        $countIps = count($campaign->coupons->where('assigned_to_ip', $request->ip()));
        $email = $collector->email;
        if($this->checkIps($campaign, $countIps) && $this->checkEmails($campaign, $countEmails, $email)){
            return true;
        }else{
            return false;

        }
    }

    public function checkIps($campaign, $ipCount)
    {
        if($campaign->filter_ips){
            if($ipCount == 0){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    public function checkEmails($campaign, $emailCount, $email)
    {
        if($emailCount == 0){
            if($campaign->filter_emails){
                if(checkmail($email)){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        }else{
            return true;
        }
    }

    public function sendClaim($campaignID, Request $request)
    {
        $r = $request->all();
        $collector = json_decode(json_encode($r['collector']));
        $campaign = Campaign::with('coupons')->find($campaignID);

        if(! $this->hasBeenClaimedByUser($collector, $campaign)){
            if(count($campaign->coupons) == $campaign->claimed){
                $this->addEmailToListAnyway($campaign, $collector);
//            $relatedItems = $this->amazon->related($campaign->asin);
                return "ended";
            }else{
                if($this->validateClaimRequest($campaign, $collector, $request)){
                    $source = $collector->source == '' ? 'none' : $collector->source;
                    $claim = $this->createClaimCode($campaignID, $collector, $source, $request);
                    Event::fire(new  ActivateClaim($collector->email, $campaign, $claim));
                    return 'success';
                }else{
                    return "error";
                }
            }
        }else{
            return "error";
        }
    }

    public function hasBeenClaimedByUser($collector, $campaign)
    {
        $countCoupons = $campaign->coupons->where('assigned_to', $collector->email)->count();
        $countClaims = $campaign->claims->where('email', $collector->email)->count();
        if($countCoupons == 0 && $countClaims == 0){
            return false;
        }else{
            return true;
        }
    }

    public function createClaimCode($campaignID, $collector, $source, $request)
    {
        $claim = new Claim;
        $claim->email = $collector->email;
        $campaign = Campaign::find($campaignID);
        if($campaign->collect_names){
            $claim->first_name = $collector->first;
            $claim->last_name = $collector->last;
        }
        $claim->code = uniqid('cc'.rand(1,10000));
        $claim->campaign_id = $campaignID;
        $claim->source = $source;
        $claim->user_ip = $request->ip();
        $claim->save();
        return $claim;
    }

    public function lookupClaim($claim_code, Amazon $amazon, Request $request)
    {
        try{
            $claim = Claim::with('campaign')->whereCode($claim_code)->first();
            $campaign = $claim->campaign;
                $coupon = $this->getTheCoupon($claim, $campaign, $request);
                if($coupon){
                    try{
                        $sources = Source::where('campaign_id', $campaign->id)->get();
                        foreach($sources as $source){
                            if(substr($source->url, -1) == $claim->source){
                                $source->count++;
                                $source->save();
                            }
                        }
                    }catch (ModelNotFoundException $m){

                    }
                    return view('promo.coupon',['coupon' => $coupon->coupon, 'amazon' => $amazon, 'campaign' => $campaign]);
                }else{
                    $relatedItems = $amazon->related($campaign->asin);
                    return view('promo.ended2', compact('relatedItems'));
                }


        }catch (ModelNotFoundException $m){
            $relatedItems = $amazon->related('B015GOLOFO');
            return view('promo.ended2', compact('relatedItems'));
        }

    }

    public function hasBeenClaimedAlready($claim, $campaign)
    {
        $count = $campaign->coupons->where('assigned_to', $claim->email)->count();
        if($count == 0){
            return false;
        }else{
            return true;
        }
    }
    private function getTheCoupon($claim, $campaign, $request)
    {
        $countCouponsCollectedBySameEmailAddress = Coupon::where('assigned_to_ip', $request->ip)->where('campaign_id', $campaign->id)->count();
        if($countCouponsCollectedBySameEmailAddress > 0){
            return Coupon::where('assigned_to_ip', $request->ip)->where('campaign_id', $campaign->id)->first();
        }else {
            if(! $claim->is_claimed){
                $coupon = $this->getNextCoupon($claim, $request);
                if($coupon){
                    event(new CouponWasClaimed($campaign, $claim->email, $claim->first_name, $claim->last_name));
                }else{
                    return false;
                }
            }else{
                $coupon = $this->getThisClaimsCoupon($campaign, $claim->email);
            }
            return $coupon;
        }
    }

    public function getNextCoupon($claim, $request)
    {
        $campaign = $claim->campaign;
        try{
            $coupon = Coupon::whereCampaign_id($campaign->id)->whereAssigned_to('')->first();
            $coupon->assigned_to = $claim->email;
            $coupon->assigned_to_ip = $request->ip();
            $campaign->claimed ++;
            $claim->is_claimed = true;
            $coupon->save();
            $campaign->save();
            $claim->save();
            return $coupon;
        }catch(ModelNotFoundException $m){
            return false;
        }

    }

    private function getThisClaimsCoupon($campaign, $email)
    {
        try{
            $coupon = Coupon::where('campaign_id', $campaign->id)
                            ->where('assigned_to', $email)->first();

            return $coupon;
        }catch(ModelNotFoundException $m){
            return false;
        }

    }

//    private function emailHasClaimedCampaign($email, $campaign)
//    {
//        $claims = Claim::whereCampaign_id($campaign->id)->get();
//        $matched = array();
//        foreach ($claims as $c){
//            if ($c->email == $email){
//                $matched[] = $c->email;
//            }
//        }
//        return $matched;
//    }

    public function addEmailToListAnyway( $campaign, $collector )
    {
        $email = $collector->email;
        $apiKey = $campaign->mc_api_key;
        $list_id = $campaign->mc_list;

        if($campaign->mc_api_key != ''){
            $this->mc->addEmailToList($email,$list_id,$apiKey);
        }elseif($campaign->aweber_list_id != ''){
            $account = $this->aweberContract->getAccount($campaign->aweber_key, $campaign->aweber_secret);
            $this->aweberContract->addEmailToList($account, $campaign->aweber_list_id, $email);
        }else{

        }
    }



    public function resend($id, Request $request)
    {
        $campaign = Campaign::find($id);
        $claim = Claim::where([
            "user_ip" => $request->ip(),
            "campaign_id" => $id,
        ])->firstorfail();
        event(new ActivateClaim($claim->email,$campaign,$claim));
        return redirect()->back()->withInput()->with('success', 'success');
    }
}
