<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('subscribed');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        if(auth()->user()->current_billing_plan != 'REVIEWER'){
            return view('home');
        }else{
            $campaigns = Campaign::where('status', 'LAUNCHED')->get();
            return view('home', compact('campaigns'));
        }

    }
}
