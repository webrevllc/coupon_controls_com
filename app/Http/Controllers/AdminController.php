<?php

namespace App\Http\Controllers;

use App\Events\Event;
use App\Events\UserCancelled;
use App\Events\UserResubscribed;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function index()
    {
        if(Gate::denies('view_admin'))
        {
            abort(401, 'Sorry, you are not authorized to view this page!');
        }
        return view('admin.admin.index');
    }

    public function reports()
    {
        if(Gate::denies('view_admin'))
        {
            abort(401, 'Sorry, you are not authorized to view this page!');
        }
        return view('admin.admin.reports');
    }

    public function settings()
    {
        $user = User::find(auth()->user()->id);
        $invoices = $user->invoices();
        return view('admin.settings', compact('invoices'));
    }

    public function updateProfile(Request $request)
    {
        $profile = $request->profile;
        $user = User::find(auth()->user()->id);
        $user->name = $profile['name'];
        $user->password = bcrypt($profile['password']);
        $user->save();
    }

    public function cancelSubscription()
    {
        $user = User::find(auth()->user()->id);
        $user->subscription('main')->cancel();
        
    }

    public function resumeSubscription()
    {
        $user = User::find(auth()->user()->id);
        $user->subscription('main')->resume();
        event(new UserCancelled($user));
    }

    public function resubscribe(Request $request)
    {
        $this->validate($request, [
            'stripeToken' => 'required',
        ]);

        $user = \Auth::user();

        $user->newSubscription('main', 'CC')->create($request->stripeToken, ['email' => $request->email, 'description' => $request->name]);

        event(new UserResubscribed($user));

        return redirect()->back()->withInput()->with('success', 'You have successfully resubscribed!');
    }

    public function updateCard(Request $request)
    {
        \Auth::user()->updateCard($request->stripeToken);
        return redirect()->back()->withInput()->with('success', 'Your credit card has been successfully updated');
    }
}
