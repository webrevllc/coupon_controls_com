<?php

namespace App\Http\Middleware;

use Closure;

class PayingCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && ! $request->user()->subscribed()) {
            flash()->error('Sorry', 'You have to pay to play');
            return redirect('/admin/profile/subscription');
        }

        return $next($request);
    }
}
