<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CampaignLaunched extends Notification
{
    use Queueable;
    /**
     * @var \App\Events\CampaignLaunched
     */
    private $event;

    /**
     * Create a new notification instance.
     *
     * @param \App\Events\CampaignLaunched $event
     */
    public function __construct(\App\Events\CampaignLaunched $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line($this->event->user->email . ' just launched a new campaign!')
                    ->line($this->event->campaign->campaign_name)
                    ->action('Check It Out!', url('/getcoupon/'.$this->event->campaign->promo_url));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
