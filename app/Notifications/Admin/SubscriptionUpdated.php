<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionUpdated extends Notification
{
    use Queueable;
    /**
     * @var SubscriptionUpdated
     */
    private $event;

    /**
     * Create a new notification instance.
     *
     * @param SubscriptionUpdated $event
     */
    public function __construct(\Laravel\Spark\Events\Subscription\SubscriptionUpdated $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Subscription Updated")
                    ->line($this->event->user->name .' '. ($this->event->user->email))
                    ->line($this->event->user->current_billing_plan );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
