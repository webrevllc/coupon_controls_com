<?php

namespace App\Notifications\Seller;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionUpdated extends Notification
{
    use Queueable;
    /**
     * @var \Laravel\Spark\Events\Subscription\SubscriptionUpdated
     */
    private $event;

    /**
     * Create a new notification instance.
     *
     * @param \Laravel\Spark\Events\Subscription\SubscriptionUpdated $event
     */
    public function __construct(\Laravel\Spark\Events\Subscription\SubscriptionUpdated $event)
    {
        //
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Subscription Updated Successfully")
                    ->line('Hey '. $this->event->user->name . ',')
                    ->action('Login To your Account', 'https://couponcontrols.com/login');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
