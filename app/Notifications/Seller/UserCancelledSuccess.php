<?php

namespace App\Notifications\Seller;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Laravel\Spark\Events\Subscription\SubscriptionCancelled;

class UserCancelledSuccess extends Notification
{
    use Queueable;
    /**
     * @var SubscriptionCancelled
     */
    private $event;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(SubscriptionCancelled $event)
    {
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Subscription Cancelled Confirmation')
                    ->line('You have successfully cancelled your subscription.')
                    ->line('It\'s been real though '. $this->event->user->name .'!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
