<?php

namespace App\Notifications\Seller;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Welcome extends Notification
{
    use Queueable;
    /**
     * @var \Laravel\Spark\Events\Subscription\UserSubscribed
     */
    private $event;

    /**
     * Create a new notification instance.
     *
     * @param \Laravel\Spark\Events\Subscription\UserSubscribed $event
     */
    public function __construct(\Laravel\Spark\Events\Subscription\UserSubscribed $event)
    {
        //
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Welcome To CouponControls.com ' . $this->event->user->name .'!')
                    ->line('We thank you for giving us the opportunity to earn your business. If there is anything we can do to improve your experience, please email us at help@couponcontrols.com')
                    ->action('Click Here To Login', 'https://couponcontrols.com/login')
                    ->line('Enjoy your stay!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
