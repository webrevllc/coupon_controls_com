<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'features';
    protected $fillable = ['campaign_id', 'feature', 'order'];

    public function campaign()
    {
        return $this->belongsTo(\App\Campaign::class);
    }
}
