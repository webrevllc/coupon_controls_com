<?php

namespace App\Mail;

use App\Campaign;
use App\Coupon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HeresYourCoupon extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Coupon
     */
    public $coupon;
    /**
     * @var Campaign
     */
    public $campaign;

    /**
     * Create a new message instance.
     *
     * @param Coupon $coupon
     * @param Campaign $campaign
     */
    public function __construct(Coupon $coupon, Campaign $campaign)
    {

        $this->coupon = $coupon;
        $this->campaign = $campaign;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Here's Your Coupon!")->view('emails.coupon');
    }
}
