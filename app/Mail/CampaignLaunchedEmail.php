<?php

namespace App\Mail;

use App\Campaign;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampaignLaunchedEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Campaign
     */
    public $campaign;

    /**
     * Create a new message instance.
     *
     * @param Campaign $campaign
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Your Campaign Has Launched!")
                    ->view('emails.campaigns.launched');
    }
}
