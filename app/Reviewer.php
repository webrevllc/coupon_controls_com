<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    protected $user_id;
    protected $email;
    protected $status;
    protected $fb_name;
    protected $fb_link;
    protected $amazon_name;
    protected $amazon_link;
    protected $amazon_email;
    protected $has_prime;
    protected $table = "reviewers";
    protected $fillable = ['user_id', 'email', 'status', 'fb_name', 'fb_link', 'amazon_name', 'amazon_link', 'amazon_email', 'has_prime'];
}
