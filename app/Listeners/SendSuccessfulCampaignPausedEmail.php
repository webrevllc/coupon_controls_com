<?php

namespace App\Listeners;

use App\Events\CampaignPaused;
use App\Mail\CampaignPausedEmail;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendSuccessfulCampaignPausedEmail
{

    /**
     * Handle the event.
     *
     * @param  CampaignPaused  $event
     * @return void
     */
    public function handle(CampaignPaused $event)
    {
//        Mail::to($event->user->email)->send(new CampaignPausedEmail($event->campaign));

        $event->user->notify(new \App\Notifications\Seller\CampaignPaused($event));
    }
}
