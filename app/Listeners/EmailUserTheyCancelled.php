<?php

namespace App\Listeners;

use App\Notifications\Seller\UserCancelledSuccess;
use \Laravel\Spark\Events\Subscription\SubscriptionCancelled;

class EmailUserTheyCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionCancelled  $event
     * @return void
     */
    public function handle(SubscriptionCancelled $event)
    {
        $event->user->notify(new UserCancelledSuccess($event));
    }
}
