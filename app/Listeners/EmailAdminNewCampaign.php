<?php

namespace App\Listeners;

use App\Events\CampaignLaunched;
use App\Mail\CampaignLaunchedEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmailAdminNewCampaign
{

    /**
     * Handle the event.
     *
     * @param  CampaignLaunched  $event
     * @return void
     */
    public function handle(CampaignLaunched $event)
    {
        Mail::to('srosenthal82@gmail.com')->send(new CampaignLaunchedEmail($event->campaign));
    }
}
