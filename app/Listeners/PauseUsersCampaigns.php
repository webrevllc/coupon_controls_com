<?php

namespace App\Listeners;

use App\Campaign;
use \Laravel\Spark\Events\Subscription\SubscriptionCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PauseUsersCampaigns
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionCancelled  $event
     * @return void
     */
    public function handle(SubscriptionCancelled $event)
    {
        $campaigns = Campaign::where('user_id', $event->user->id)->get();

        foreach($campaigns as $c){
            $c->status = "PAUSED";
            $c->save();
        }
    }
}
