<?php

namespace App\Listeners;

use App\User;
use \Laravel\Spark\Events\Subscription\SubscriptionUpdated;

class SubscriptionUpdatedNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionUpdated  $event
     * @return void
     */
    public function handle(SubscriptionUpdated $event)
    {
        $admin = User::where('email', 'srosenthal82@gmail.com')->first();

        $admin->notify(new \App\Notifications\Admin\SubscriptionUpdated($event));

        $event->user->notify(new \App\Notifications\Seller\SubscriptionUpdated($event));
    }
}
