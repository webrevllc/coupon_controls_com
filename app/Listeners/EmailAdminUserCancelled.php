<?php

namespace App\Listeners;

use App\User;
use App\Notifications\Admin\UserCancelled;
use Laravel\Spark\Events\Subscription\SubscriptionCancelled;

class EmailAdminUserCancelled
{
    /**
     * Handle the event.
     *
     * @param  SubscriptionCancelled  $event
     * @return void
     */
    public function handle(SubscriptionCancelled $event)
    {

            $admin = User::where('email', 'srosenthal82@gmail.com')->first();

            $admin->notify(new UserCancelled($event));
    }
}
