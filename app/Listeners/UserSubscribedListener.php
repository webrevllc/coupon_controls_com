<?php

namespace App\Listeners;

use App\User;
use Laravel\Spark\Events\Subscription\UserSubscribed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSubscribedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserSubscribed  $event
     * @return void
     */
    public function handle(UserSubscribed $event)
    {
        $admin = User::where('email', 'srosenthal82@gmail.com')->first();

        $admin->notify(new \App\Notifications\Admin\Welcome($event));

        $event->user->notify(new \App\Notifications\Seller\Welcome($event));
    }
}
