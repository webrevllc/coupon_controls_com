<?php

namespace App\Listeners;

use App\Events\CampaignLaunched;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSuccessfulCampaignLaunchedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CampaignLaunched  $event
     * @return void
     */
    public function handle(CampaignLaunched $event)
    {
//        Mail::to($event->user->email)->send(new CampaignLaunchedEmail($event->campaign));

        $admin = User::where('email', 'srosenthal82@gmail.com')->first();
        $admin->notify(new \App\Notifications\Admin\CampaignLaunched($event));
        $event->user->notify(new \App\Notifications\Seller\CampaignLaunched($event));

    }
}
