<?php

namespace App\Providers;

use Carbon\Carbon;
use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'WebRev LLC',
        'product' => 'CouponControls.com',
        'street' => '30030 Messara Cove',
        'location' => 'Mount Dora, FL 32757',
        'phone' => '407-312-9455',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = 'help@couponcontrols.com';

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'srosenthal82@gmail.com'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
//        Spark::collectBillingAddress();
        Spark::promotion('coupon-code');
        Spark::plan('Standard', 'CC-Standard-M')
            ->price(39.95)
            ->features([
                'Unlimited Campaigns', 'Unlimited Coupons', 'Integrates with Mailchimp/Aweber', 'Dozens of Other Features'
            ]);
        Spark::plan('Standard', 'CC-M-P')
            ->price(199.95)
            ->features([
                'Unlimited Campaigns', 'Unlimited Coupons', 'Integrates with Mailchimp/Aweber', 'Dozens of Other Features'
            ]);
        Spark::plan('Standard', 'CC-Y-P')
            ->price(999.95)
            ->yearly()
            ->features([
                'Get 1 Month Free!'
            ]);
        Spark::plan('Standard', 'CC-Standard-Y')
            ->price(439.45)
            ->yearly()
            ->features([
                'Get 1 Month Free!'
            ]);
//        Spark::plan('Pro', 'CC-Pro-M')
//            ->price(89.95)
//            ->features([
//                'Same as Standard plus your campaigns will be displayed to over 5,000 CouponControl reviewers - FREE TRAFFIC!!'
//            ]);
//        Spark::plan('Pro', 'CC-Pro-Y')
//            ->price(899.50)
//            ->yearly()
//            ->features([
//                'Get 2 Months Free!'
//            ]);
//        Spark::plan('VIP-Reviewer', 'REVIEWER-VIP')
//            ->price(10)
//            ->features([
//                'Access To Every Promotion'
//            ]);


    }
}
