<?php

namespace App\Providers;

use ApaiIO\ApaiIO;
use ApaiIO\Operations\Lookup;
use App\Contracts\Amazon;
use App\Services\AmazonLookup;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Amazon::class, function(){
            $conf = new \ApaiIO\Configuration\GenericConfiguration;
            $conf
                ->setCountry('com')
                ->setAccessKey(env('AMAZON_ACCESS_KEY'))
                ->setSecretKey(env('AMAZON_SECRET_KEY'))
                ->setAssociateTag(env('AMAZON_ASSOCIATE_TAG'));
            $lookup = new Lookup;
            $apaio = new ApaiIO($conf);

            return new  AmazonLookup($apaio, $lookup);
        });

        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
    }
}
