<?php

namespace App\Providers;

use App\Contracts\AweberContract;
use App\Services\AweberClass;
use AWeberAPI;
use Illuminate\Support\ServiceProvider;

class AweberProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AweberContract::class, function() {
            $consumerKey = 'AkkRdx2CUWq6fqwmSX3gpK4L';
            $consumerSecret = 'u4GCtdibdeuvqYv0Za4qQ6Wtq7bkkXg2qnvvWi7o';
            return new AweberClass(new AWeberAPI($consumerKey, $consumerSecret)) ;
        });
    }
}
