<?php
namespace App\Services;

use App\Contracts\AweberContract;
use AWeberAPI;
use AWeberAPIException;

class AweberClass implements AweberContract
{

    /**
     * @var AWeberAPI
     */
    private $aweber;

    public function __construct(AWeberAPI $aweber)
    {
        $this->aweber = $aweber;
    }


    public function display_access_tokens()
    {
        if (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])){

            $this->aweber->user->requestToken = $_GET['oauth_token'];
            $this->aweber->user->verifier = $_GET['oauth_verifier'];
            $this->aweber->user->tokenSecret = $_COOKIE['secret'];

            list($accessTokenKey, $accessTokenSecret) = $this->aweber->getAccessToken();
            $account = $this->getAccount($accessTokenKey, $accessTokenSecret);
            return array(
                'key' => $accessTokenKey,
                'secret' => $accessTokenSecret,
                'account_id' => $account->id
            );
        }
        $callbackURL = $this->get_self();
        list($key, $secret) = $this->aweber->getRequestToken($callbackURL);
        $authorizationURL = $this->aweber->getAuthorizeUrl();

        setcookie('secret', $secret);

        header("Location: $authorizationURL");
        exit();
    }

    public function getAccount($accessKey, $accessSecret)
    {
        try
        {
            $account = $this->aweber->getAccount($accessKey, $accessSecret);
            return $account;

        } catch(AWeberAPIException $exc)
        {
            echo $exc->message;
            exit(1);
        }
    }

    private function get_self(){
        return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    public function display_available_lists($account){
        $listURL ="/accounts/{$account->id}/lists/";
        $lists = $account->loadFromUrl($listURL);
        return $lists->data['entries'];
    }

    public function addEmailToList($account, $list_id, $email)
    {
        $account_id = $account->id;
        $listURL = "/accounts/{$account_id}/lists/{$list_id}";
        $list = $account->loadFromUrl($listURL);
        $subscribers = $list->subscribers->create(['email' => $email]);

    }

    public function addEmailAndUserToList($account, $list_id, $email, $name)
    {
        $account_id = $account->id;
        $listURL = "/accounts/{$account_id}/lists/{$list_id}";
        $list = $account->loadFromUrl($listURL);
        $subscribers = $list->subscribers->create(['email' => $email, 'name' => $name]);

    }


}