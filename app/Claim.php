<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $fillable =['campaign_id', 'email', 'code', 'is_claimed'];

    public function campaign()
    {
        return $this->belongsTo('\App\Campaign');
    }

    public function claimed()
    {
        return $this->is_claimed;
    }
}
