<?php

namespace App;

use App\Task;
use Laravel\Spark\User as SparkUser;

class User extends SparkUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];

    public function routeNotificationForSlack()
    {
        return 'https://hooks.slack.com/services/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';
    }

    public function routeNotificationForNexmo()
    {
        return '14073129455';
    }

    public function campaigns()
    {
        return $this->hasMany('\App\Campaign');
    }

}
