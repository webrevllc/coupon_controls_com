<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    protected $table = 'coupons';
    protected $fillable = ['campaign_id', 'coupon', 'assigned_to', 'campaign_name'];

    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }
}
