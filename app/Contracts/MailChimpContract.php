<?php

namespace App\Contracts;

interface MailChimpContract
{
    public function getLists($apikey);
    public function addEmailToList($email, $listID, $apiKey);
    public function addEmailAndUserToList($email, $firstName, $lastName, $listID, $apiKey);

}