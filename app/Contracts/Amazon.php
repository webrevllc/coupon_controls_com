<?php namespace App\Contracts;

use \ApaiIO\Configuration\GenericConfiguration;

interface Amazon {

    public function returnAmazonImages($asin);
    public function returnProductName($asin);
    public function returnFeatures($asin);
    public function getDescription($asin);
    public function getTitle($asin);
    public function getPrice($asin);
    public function reviews($asin);
    public function related($asin);
    public function myLookup($asin);
    public function productUrl($asin);
    public function getXML($asin);

}