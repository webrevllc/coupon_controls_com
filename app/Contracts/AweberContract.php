<?php namespace App\Contracts;

interface AweberContract {
    public function display_access_tokens();
    public function getAccount($key, $secret);
    public function display_available_lists($account);
    public function addEmailToList($account, $list_id, $email);
    public function addEmailAndUserToList($account, $list_id, $email, $name);
}